# -*- coding: utf-8 -*-

from array import array
import csv

from chartable import ct, dakuten, maru, reversetable

romfilename = "Kattobi! Takuhaikun (J) - Copy.pce"
hasheader = False
headersize = 0x200
pointertablestart = 0x18000
pointertablelength = 0x510
if hasheader:
    pointertablestart = pointertablestart + headersize

datatablestart = 0x18510
datatablelength = 0x4f70
if hasheader:
    datatablestart = datatablestart + headersize
    
rows = []
with open("kattobi.csv", "r", encoding="utf-8-sig") as csvf:
    csvreader = csv.DictReader(csvf)
    for row in csvreader:
        rows.append(row)

pt = array("H")        
dt = array("B")

for row in rows:
    text = row["translation"]
    val = 0
    pointer = datatablestart - 0x10000 + len(dt) + 0x2000
    if int(row["DI"]) != -1:
        pointer = pt[int(row["DI"])]
        pt.append(pointer)
        continue
    pt.append(pointer)
    for c in text:
        if c == "\n":
            val = 0xFE
        else:
            val = reversetable[c.upper()]
        dt.append(val)
    if int(row["FD"]) == 1:
        dt.append(0xFD)
    dt.append(0xFF)
    
with open(romfilename, "r+b") as f:
    #write text block and pointer table
    f.seek(pointertablestart)
    f.write(pt)
    f.write(dt)
    
    #write yes/no boxes
    yes = array("H")
    yes.append(0xE129) #Y
    yes.append(0xE115) #E
    yes.append(0xE123) #S
    
    no = array("H")
    no.append(0xE11E) #N
    no.append(0xE11F) #O
    no.append(0xE110) #whitespace
    
    f.seek(0x25916) #yes in shop
    f.write(yes)
    f.seek(0x25932) #no in shop
    f.write(no)
    
    #replace item names
    pkg = array("H")
    pkg.append(0xe120) #P
    pkg.append(0xe11B) #K
    pkg.append(0xe117) #G
    
    f.seek(0x260B8) #nimotsu A
    f.write(pkg)
    f.seek(0x260C4) #nimotsu B
    f.write(pkg)
    f.seek(0x260D0) #nimotsu C
    f.write(pkg)
    f.seek(0x260DC) #nimotsu D
    f.write(pkg)
    
    
    #replace bonus
#    bonus = array("H")
#    bonus.append(0xe112) #B
#    bonus.append(0xe11f) #O
#    bonus.append(0xe11e) #N
#    bonus.append(0xe125) #U
#    bonus.append(0xe123) #S
#    bonus.append(0xffff) #hope this works????
    tip = array("H")
    tip.append(0xe124) #T
    tip.append(0xe119) #I
    tip.append(0xe120) #P
    tip.append(0xe110) #whitespace
    f.seek(0x26DF4)
    #f.write(bonus)
    f.write(tip)
    
    #bonus - eliminate dakuten
    blank = array("H")
    blank.append(0xe110) #whitespace
    f.seek(0x26DF0)
    f.write(blank)
    
    #replace fine
    fine = array("H")
    fine.append(0xe116)
    fine.append(0xe119)
    fine.append(0xe11E)
    fine.append(0xe115)
    f.seek(0x260EC)
    f.write(fine)
    
    #fine - replace dakuten
    f.seek(0x260E8)
    f.write(blank)
    