# -*- coding: utf-8 -*-
from array import array
import csv
import json

from chartable import ct, dakuten, maru

romfilename = "Kattobi! Takuhaikun (J).pce"
hasheader = False
headersize = 0x200
pointertablestart = 0x18000
pointertablelength = 0x510
if hasheader:
    pointertablestart = pointertablestart + headersize

datatablestart = 0x18510
datatablelength = 0x4f70
if hasheader:
    datatablestart = datatablestart + headersize

with open(romfilename, "rb") as f:
    f.seek(pointertablestart)
    pt = array("H")
    pt.fromfile(f, int(pointertablelength / pt.itemsize))
    f.seek(datatablestart)
    dt = array("B")
    dt.fromfile(f, int(datatablelength / dt.itemsize))
 
strings = []   
hasFD = []
for o in pt:
    to = o - 0x2000
    text = ""
    for c in dt[to - (datatablestart - 0x10000) :]:
        fd = 0
        if c == 0xfd:
            fd = 1
            break
        elif c == 0xff:
            break
        elif c == 0xfe:
            text = text + "\n"
        elif c == 0x8e:
            text = text[: len(text) - 1] + dakuten[text[-1]]
        elif c == 0x8f:
            text = text[: len(text) - 1] + maru[text[-1]]
        else:
            text = text + (ct[c])
    strings.append(text)
    hasFD.append(fd)
    
with open("kattobi.txt", "w", encoding="utf-8-sig") as tf:
    tf.write(romfilename + " text dump\n")
    tf.write("=================================================\n")
    for i in range(len(pt)):
        o = pt[i]
        to = o - 0x2000
        entry = strings[i]
        tf.write("index: " + str(i) +"/" + str(len(pt) - 1) + "\n")
        tf.write("logical offset: " + hex(o) + "\n")
        tf.write("physical offset: " + hex(to + 0x10000) + "\n")
        tf.write("text entry:\n" + entry + "\n")
        tf.write("=================================================\n")

with open("kattobi.csv", "w", encoding="utf-8-sig") as csvf:
    csvwriter = csv.writer(csvf, dialect="unix")
    csvlist = []
    csvwriter.writerow(["index", "FD", "DI", "entry", "translation"])
    for i in range(len(pt)):
        o = pt[i]
        if pt.index(o) < i:
            duplicate = pt.index(o)
        else:
            duplicate = -1
        to = o - 0x2000
        entry = strings[i]
        #transl = str(i) + " :XXXXXXXX\nXXXXXXXX\nXXXX"
        row = [i, hasFD[i], duplicate, entry]
        csvlist.append(row)
    csvwriter.writerows(csvlist)
        
with open("kattobi2.csv", "w", encoding="utf-8-sig") as csvf:
    csvwriter = csv.writer(csvf)
    csvlist = []
    for i in range(len(pt)):
        o = pt[i]
        to = o - 0x2000
        entry = strings[i]
        row = [hex(o), hex(to + 0x10000), entry]
        csvlist.append(row)
    csvwriter.writerows(csvlist)
    
with open("kattobi.json", "w", encoding="utf-8-sig") as jf:
    jsonlist = []
    for i in range(len(pt)):
        jsonentry = {}
        jsonentry["Index"] = i
        jsonentry["fd"] = hasFD[i]
        jsonentry["original"] = strings[i]
        jsonentry["translated"] = ""
        jsonlist.append(jsonentry)
    json.dump(jsonlist, jf, ensure_ascii=False, indent=4, separators=(',', ': '), sort_keys=True)