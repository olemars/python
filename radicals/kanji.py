# -*- coding: utf-8 -*-

import json
import csv
import xml.etree.ElementTree as ET
import codecs

#https://www.wanikani.com/api/user/apikey/kanji/1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50

jsonfile = json.load(open("kanji.txt", encoding="utf-8-sig"))
wkentries = jsonfile["requested_information"]

kangxireader = csv.reader( open("kangxi.csv", encoding="utf-8-sig"))
kangxichars = []
for row in kangxireader:
    kangxichars.append(row[1])
    
    
kdtree = ET.parse('kanjidic2.xml')
kdroot = kdtree.getroot()

kdentries = []
for character in kdroot.iter("character"):
    kdentry = {}
    kdentry["character"] = character.find("literal").text
    
    radical = character.find("radical")
    for radvalue in radical.iter("rad_value"):
        if radvalue.get("rad_type") == "classical":
            kdentry["radical_index"] = int(radvalue.text)
            kdentry["radical_symbol"] = kangxichars[int(radvalue.text) - 1]
            
    misc = character.find("misc")
    if misc.find("grade") is not None:
        kdentry["grade"] = int(misc.find("grade").text)
#    if misc.find("jlpt") is not None:
#        kdentry["jlpt"] = int(misc.find("jlpt").text)
#    if misc.find("freq") is not None:
#        kdentry["freq"] = int(misc.find("freq").text)
        
    reading_meaning = character.find("reading_meaning")
    if reading_meaning is not None:
        rmgroup = reading_meaning.find("rmgroup")
        if rmgroup is not None:
            on = []
            kun = []
            for reading in rmgroup.iter("reading"):
                if reading.get("r_type") == "ja_on":
                    on.append(reading.text)
                if reading.get("r_type") == "ja_kun":
                    kun.append(reading.text)
            kdentry["on"] = on
            kdentry["kun"] = kun
            
            meanings = []
            for meaning in rmgroup.iter("meaning"):
                if meaning.get("m_lang") == None or meaning.get("m_lang") == "en":
                    meanings.append(meaning.text)
            kdentry["meaning"] = meanings
                
    kdentries.append(kdentry)
        
with open("kanjidic.txt", "w", encoding="utf-8-sig", newline='') as kdjson:
    json.dump(kdentries, kdjson, ensure_ascii=False)
    
wkkdentries = []
for kdentry in kdentries:
    for wkentry in wkentries:
        if kdentry["character"] == wkentry["character"]:
            wkkdentries.append(kdentry)
            break
        
joyoentries = []
for kdentry in kdentries:
    if "grade" in kdentry and kdentry["grade"] < 9:
        joyoentries.append(kdentry)
