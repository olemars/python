# -*- coding: utf-8 -*-

#api command https://www.wanikani.com/api/user/apikey/radicals/1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50

import json
import csv
import codecs

radfile = open("radicals.txt", encoding="utf-8-sig")
jsonfile = json.load(radfile)
wkchars = []
wkimages = []
wkmeanings = []
for entry in jsonfile["requested_information"]:
    wkchars.append(entry["character"])
    wkimages.append(entry["image"])
    wkmeanings.append(entry["meaning"])

kangxifile = open("kangxi.csv", encoding="utf-8-sig")
kangxireader = csv.reader(kangxifile)
kangxichars = []
kangximeanings = []
for row in kangxireader:
    kangxichars.append(row[1])
    kangximeanings.append(row[2].replace("?", ""))
    
comp = []
for x in range(len(kangxichars)):
    entry = []
    entry.append(kangxichars[x])
    entry.append(kangximeanings[x])
    for y in range(len(wkchars)):
        if str(wkchars[y]) in str(kangxichars[x]):
            entry.append(wkchars[y])
            entry.append(wkmeanings[y])
            if wkimages[y] is not None:
                entry.append(wkimages[y])
    comp.append(entry)

with open("comp.csv", "w", encoding="utf-8-sig", newline='') as cf:
    writer = csv.writer(cf)
    writer.writerows(comp)
  

noncomp = []
for y in range(len(wkchars)):
    flagged = False
    for x in range(len(kangxichars)):  
        if wkchars[y] == None:
            flagged = False
        elif str(wkchars[y]) in str(kangxichars[x]):
            flagged = True
    if not flagged:
        nonentry = []
        nonentry.append(wkchars[y])
        nonentry.append(wkimages[y])
        nonentry.append(wkmeanings[y])
        noncomp.append(nonentry)
  
with open("noncomp.csv", "w", encoding="utf-8-sig", newline='') as ncf:
    nonwriter = csv.writer(ncf)
    nonwriter.writerows(noncomp)
    

with open("wk.csv", "w", encoding="utf-8-sig", newline='') as wkf:
    wkwriter = csv.writer(wkf)
    wklist = []
    for y in range(len(wkchars)):
        row = [wkchars[y], wkmeanings[y], wkimages[y]]
        wklist.append(row)
    wkwriter.writerows(wklist)