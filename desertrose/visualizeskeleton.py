# -*- coding: utf-8 -*-

import os
import numpy as np
import matplotlib as mpl
from matplotlib.widgets import Slider
#import scipy
from scipy import ndimage
import pylab
import matplotlib.pyplot as plt
from skimage.morphology import medial_axis
from skimage.filter import roberts, sobel
#import pymorph
#import mahotas


#3rdparty from http://nedbatchelder.com/blog/200712/human_sorting.html
import re

def tryint(s):
    try:
        return int(s)
    except:
        return s
     
def alphanum_key(s):
    """ Turn a string into a list of string and number chunks.
        "z23a" -> ["z", 23, "a"]
    """
    return [ tryint(c) for c in re.split('([0-9]+)', s) ]

def sort_nicely(l):
    """ Sort the given list in the way that humans expect.
    """
    l.sort(key=alphanum_key)
#end 3rdparty
    

imglist1 = ["Direction 1/" + f for f in os.listdir("Direction 1") if f.endswith(".jpg")]
imglist2 = ["Direction 2/" + f for f in os.listdir("Direction 2") if f.endswith(".jpg")]
imglist3 = ["Direction 3/" + f for f in os.listdir("Direction 3") if f.endswith(".jpg")]
sort_nicely(imglist1)
sort_nicely(imglist2)
sort_nicely(imglist3)

top = 80
bottom = 870
left = 460
right = 1180
stride = 2

start1 = 39
start2 = 15
start3 = 22

imglist = imglist3[::stride]
start = start3

baseimg = ndimage.imread(imglist[0], True).astype("uint8")

imgarray = np.empty([len(imglist), len(baseimg[top:bottom:stride,0]), len(baseimg[0,left:right:stride])], dtype="uint8") 
num = 0
for fn in imglist:
    img = ndimage.imread(fn, True, )[top:bottom:stride, left:right:stride].astype("uint8")
    imgarray[num] = img
    num += 1


ax = pylab.subplot(111)
pylab.subplots_adjust(left=0.2, bottom=0.2)

frame = start
nframes = imgarray.shape[0] + start
currimg = imgarray[frame - start]
mask = currimg > currimg.mean()
skel, distance = medial_axis(currimg, mask=mask, return_distance=True)
dist_on_skel = distance * skel
l = pylab.imshow(dist_on_skel, cmap=plt.cm.jet)
pylab.set_cmap("jet")

axcolor = 'lightgoldenrodyellow'
axframe = pylab.axes([0.35, 0.1, 0.5, 0.03], axisbg=axcolor)
sframe = Slider(axframe, 'Frame', start, nframes - 1, valinit=start,valfmt='%1d'+'/'+str(nframes - 1))

def update(val):
    frame = np.around(sframe.val)
    currimg = imgarray[frame - start]
    mask = currimg > currimg.mean()
    skel, distance = medial_axis(currimg, mask=mask, return_distance=True)
    dist_on_skel = roberts(currimg) + distance * skel
    l.set_data(dist_on_skel)
    pylab.set_cmap("jet")
    
    
    
sframe.on_changed(update)

#pylab.gray()
pylab.show()# -*- coding: utf-8 -*-

