# -*- coding: utf-8 -*-

import os
import numpy as np
#import scipy
from scipy import ndimage
#import pymorph
#import mahotas


#easy_install pyqtgraph
from pyqtgraph.Qt import QtCore, QtGui
import pyqtgraph.opengl as gl

app = QtGui.QApplication([])
w = gl.GLViewWidget()
w.opts['distance'] = 500
w.show()
w.setWindowTitle('test')

#b = gl.GLBoxItem()
#w.addItem(b)
g = gl.GLGridItem()
g.scale(1, 1, 1)
w.addItem(g)


#3rdparty from http://nedbatchelder.com/blog/200712/human_sorting.html
import re

def tryint(s):
    try:
        return int(s)
    except:
        return s
     
def alphanum_key(s):
    """ Turn a string into a list of string and number chunks.
        "z23a" -> ["z", 23, "a"]
    """
    return [ tryint(c) for c in re.split('([0-9]+)', s) ]

def sort_nicely(l):
    """ Sort the given list in the way that humans expect.
    """
    l.sort(key=alphanum_key)
#end 3rdparty
    

imglist1 = ["Direction 1/" + f for f in os.listdir("Direction 1") if f.endswith(".jpg")]
imglist2 = ["Direction 2/" + f for f in os.listdir("Direction 2") if f.endswith(".jpg")]
imglist3 = ["Direction 3/" + f for f in os.listdir("Direction 3") if f.endswith(".jpg")]
sort_nicely(imglist1)
sort_nicely(imglist2)
sort_nicely(imglist3)

top = 80
bottom = 870
left = 460
right = 1180
stride = 2

start1 = 39
start2 = 15
start3 = 22

imglist = imglist1[::stride]
start = start1

baseimg = ndimage.imread(imglist[0], True).astype("uint8")

imgarray = np.empty([len(imglist), len(baseimg[top:bottom:stride,0]), len(baseimg[0,left:right:stride])], dtype="uint8") 
num = 0
for fn in imglist:
    img = ndimage.imread(fn, True, )[top:bottom:stride, left:right:stride].astype("uint8")
    imgarray[num] = img
    num += 1
    
d2 = np.empty(imgarray.shape + (4,), dtype=np.ubyte)
d2[..., 0] = imgarray
d2[..., 1] = imgarray
d2[..., 2] = imgarray
d2[..., 3] = d2[..., 0]*0.3 + d2[..., 1]*0.3
d2[..., 3] = (d2[..., 3].astype(float) / 255.) **2 * 255

d2[:, 0, 0] = [255,0,0,100]
d2[0, :, 0] = [0,255,0,100]
d2[0, 0, :] = [0,0,255,100]

v = gl.GLVolumeItem(d2)
v.translate(-200,-200,-100)
w.addItem(v)

#ax = gl.GLAxisItem()
#w.addItem(ax)

if __name__ == '__main__':
    import sys
    if (sys.flags.interactive != 1) or not hasattr(QtCore, 'PYQT_VERSION'):
        QtGui.QApplication.instance().exec_()